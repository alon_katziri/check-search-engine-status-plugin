<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Check_Site_Search_Status
 * @subpackage Check_Site_Search_Status/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Check_Site_Search_Status
 * @subpackage Check_Site_Search_Status/includes
 * @author     Your Name <email@example.com>
 */
class Check_Site_Search_Status_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}

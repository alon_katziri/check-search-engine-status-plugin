<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Check_Site_Search_Status
 * @subpackage Check_Site_Search_Status/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Check_Site_Search_Status
 * @subpackage Check_Site_Search_Status/includes
 * @author     Your Name <email@example.com>
 */
class Check_Site_Search_Status_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}

<?php

/**
 * The plugin bootstrap file
 *
 *
 * @link              http://illuminea.com
 * @since             1.0.0
 * @package           Check_Site_Search_Status
 *
 * @wordpress-plugin
 * Plugin Name:       Site search Status
 * Plugin URI:        http://illminea.com/
 * Description:       Tells you if your site is open or closed for search engines
 * Version:           1.0.0
 * Author:            illuminea
 * Author URI:        http://example.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       site-seach-stutas
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-name-activator.php
 */
function activate_Check_Site_Search_Status() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-site-search-stutas-activator.php';
	Check_Site_Search_Status_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-plugin-name-deactivator.php
 */
function deactivate_Check_Site_Search_Status() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-site-search-stutas-deactivator.php';
	Check_Site_Search_Status_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_Check_Site_Search_Status' );
register_deactivation_hook( __FILE__, 'deactivate_Check_Site_Search_Status' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-site-search-stutas.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_Check_Site_Search_Status() {

	$plugin = new Check_Site_Search_Status();
	$plugin->run();

}
run_Check_Site_Search_Status();
